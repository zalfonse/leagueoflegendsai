from structure import *

class Team:
    
    def __init__(self):        
        # End Game Objective
        self.nexus = Nexus(1)
        self.nexustower1 = Tower(4,1)
        self.nexustower2 = Tower(4,1)
        
        #Inhibitors
        self.inhibitors = [Inhibitor(2),Inhibitor(1),Inhibitor(0)]
        
        # Lane Stacks
        self.botstack = [Tower(3,0),Tower(2,0),Tower(1,0)]
        self.midstack = [Tower(3,1),Tower(2,1),Tower(1,1)]
        self.topstack = [Tower(3,2),Tower(2,2),Tower(1,2)]


    #Tower Functions
    def getTowers(self):
        available = []
        
        if self.midstack:
            available.append(self.midstack[-1])
        if self.topstack:
            available.append(self.topstack[-1])
        if self.botstack:
            available.append(self.botstack[-1])
        
        return available
    
    def topTowerDown(self):
        return self.topstack.pop()
    
    def midTowerDown(self):
        return self.midstack.pop()
    
    def botTowerDown(self):
        return self.botstack.pop()
    
    
    #Inhibitor Function
    def getInhibs(self):
        available = []
        
        if not self.topstack:
            available.append(self.inhibitors[0])
        if not self.midstack:
            available.append(self.inhibitors[1])
        if not self.botstack:
            available.append(self.inhibitors[2])
            
        return available 

    
    def getAllTargets(self):
        return self.getInhibs() + self.getTowers()
    
    
    def getHighestPriority(self):
        return max(self.getAllTargets(),key=lambda l: l.value)


class LeagueMap: 

    gameID = "1337"
    homeTeam = Team()
    enemyTeam = Team()
    
    
    def play(self):
        print "Defend friendly " + str(self.homeTeam.getHighestPriority())
        print "Attack enemy " + str(self.enemyTeam.getHighestPriority())