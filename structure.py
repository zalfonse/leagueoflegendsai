class Tower():
    #Tier 1 is outermost
    #Tier 2 is middle tower
    #Tier 3 is innermost
    #Tier 4 is nexus tower
    #Location 0 is bot
    #Location 1 is mid/nexus
    #location 2 is top
    def __init__(self, tier, location, hp=0, dps=0):
            self.hp   = hp
            self.dps  = dps 
            self.tier = tier
            self.location = location
            self.value = self.calcValue()
            
    def calcValue(self):
            if self.tier == 1:
                return 15
            if self.tier == 2:
                return 20
            if self.tier == 3:
                return 25
            if self.tier == 4: 
                return 35
            else:
                return -1

    def __rep__(self):
            return self.__str__()
    def __str__(self):
            return ("Top " if self.location == 2 else "Mid " if self.location == 1 else "Bot " if self.location == 0 else "") + "Tier " + str(self.tier) + " Tower"

    
    
class Inhibitor():
    def __init__(self, location, hp=0):
            self.location = location
            self.hp   = hp
            self.value = self.calcValue()
            
    def calcValue(self):
            return 30
    
    def __rep__(self):
            return self.__str__()
    def __str__(self):
            return ("Top " if self.location == 2 else "Mid " if self.location == 1 else "Bot " if self.location == 0 else "") + "Inhibitor"

class Nexus():
    
    def __init__(self, zone, hp=0):
            self.hp   = hp
            self.zone = zone
            self.value = self.calcValue()
            
    def calcValue(self):
            if self.zone == 31:
                return 40
            if self.zone == 1:
                return 41