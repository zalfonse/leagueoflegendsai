class Monster:
    
    value   = 0
    hp      = 0
    dps     = 0
    zone    = 0
    
class Baron(Monster):
    
    def __init__(self, dps=0, hp=0):
        self.value = 35
        self.hp  = hp
        self.dps = dps


class Dragon(Monster):
    
    def __init__(self, dps=0, hp=0):
        self.value = 20
        self.hp = hp
        self.dps = dps
        
        
class RedBuff(Monster):
    
    def __init__(self, zone, dps=0, hp=0):
        self.dps = dps
        self.hp  = hp
        self.zone= zone
        self.value = self.calcValue()
        
    def calcValue(self):
        if self.zone == 12:
            return 11
        if self.zone == 20:
            return 10
        
        
class BlueBuff(Monster):
    
    def __init__(self, zone, dps=0, hp=0):
        self.dps = dps
        self.zone = zone
        self.hp = hp
        self.value = self.calcValue()
        
        
    def calcValue(self):
        if self.zone == 10:
            return 11
        if self.zone == 22:
            return 10
        
        
class SmallCamp(Monster):

    def __init__(self, zone, dps=0, hp=0):
        self.dps = dps
        self.hp = hp
        self.zone = zone
        self.value = 5
        
        
        